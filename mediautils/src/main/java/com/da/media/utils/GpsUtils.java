package com.da.media.utils;

import com.drew.lang.GeoLocation;

public class GpsUtils {

    /**
     * Example: https://www.google.es/maps/place/{lat},{lon}
     * @param geoLocation
     * @return
     */
    public static String buildMapsUrl(GeoLocation geoLocation) {
        double lat=geoLocation.getLatitude();
        double lon=geoLocation.getLongitude();
        String url="https://www.google.es/maps/place/"+lat+"%20"+lon;
        return url;
    }


    /**
     * examples:
     * lat1: 41.423801499999996, lon1: 2.1824180833333333
     * @param geoLocation1
     * @param geoLocation2
     * @return
     */
    public static double distance(GeoLocation geoLocation1, GeoLocation geoLocation2) {
        double lat1=geoLocation1.getLatitude();
        double lon1=geoLocation1.getLongitude();
        double lat2=geoLocation2.getLatitude();
        double lon2=geoLocation2.getLongitude();
        return distance(lat1, lon1, lat2, lon2, 'K');
    }

    /**
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @param unit  M (default) for Miles, K for kilometers, N for Nautical miles.
     * @return
     */
    private static double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515; // Miles
        if (unit == 'K') {
            dist = dist * 1.609344; // Kilometers
        } else if (unit == 'N') {
            dist = dist * 0.8684; // Nautical miles
        }
        return (dist);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
}
