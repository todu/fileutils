package com.da.media.utils;

import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.file.FileSystemDirectory;

import java.io.File;
import java.util.Collection;
import java.util.Date;

public class MetadataUtils {

    public Date getFileDate(String fileName) {
        Date fileDate=null;
        try {
            File file = new File(fileName);
            Metadata metadata = com.drew.imaging.ImageMetadataReader.readMetadata(file);
            Collection<?> dirs=metadata.getDirectoriesOfType(ExifSubIFDDirectory.class);
            Date oneDate=getDateFromDirectory(dirs, null);
            if (oneDate==null) {
                // image capture date
                dirs=metadata.getDirectoriesOfType(ExifIFD0Directory.class);
                oneDate=getDateFromDirectory(dirs, null);
            }
            if (oneDate==null) {
                // gps date
                dirs=metadata.getDirectoriesOfType(GpsDirectory.class);
                oneDate=getDateFromDirectory(dirs, null);
            }
            if (oneDate==null) {
                // file system modification date
                dirs=metadata.getDirectoriesOfType(FileSystemDirectory.class);
                oneDate=getDateFromDirectory(dirs, new int[]{3});
            }
            fileDate=oneDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileDate;
    }

    /**
     * https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html
     * Tag ID	Tag Name	Writable	Group	Values / Notes
     * 0x010f	Make	string	IFD0
     *    0x010f = 271
     * 0x0110	Model	string	IFD0
     *    0x0110 = 272
     * 0x0132	ModifyDate	string	IFD0	(called DateTime by the EXIF spec.)
     *    0x0132 = 306
     * 0x82aa	MDPrepDate	no	-
     *    0x82aa = 33450
     * 0x882a	TimeZoneOffset	int16s[n]	ExifIFD	(1 or 2 values: 1. The time zone offset of DateTimeOriginal from GMT in hours, 2. If present, the time zone offset of ModifyDate)
     *    0x882a = 34858
     * 0x9003	DateTimeOriginal	string	ExifIFD	(date/time when original image was taken)
     *    0x9003 = 36867
     * 0x9004	CreateDate	string	ExifIFD	(called DateTimeDigitized by the EXIF spec.)
     *    0x9004 = 36868
     * 0x9010	OffsetTime	string	ExifIFD	(time zone for ModifyDate)
     *    0x9010 = 36880
     * 0x9011	OffsetTimeOriginal	string	ExifIFD	(time zone for DateTimeOriginal)
     *    0x9011 = 36881
     * 0x9012	OffsetTimeDigitized	string	ExifIFD	(time zone for CreateDate)
     *    0x9012 = 36882
     * 0xc71b	PreviewDateTime	string!	IFD0
     *    0xc71b = 50971
     *
     * In FileSystemDirectory, type 3 can describe modified date
     */
    private Date getDateFromDirectory(Collection<?> directories, int[]extraKeys) {
        final int[]genericKeys={29, 0x0132, 0x9003, 0x9004, 0x82aa};
        int[]keys;
        if (extraKeys==null) {
            keys=genericKeys;
        } else {
            int genlen=genericKeys.length;
            int extralen=extraKeys.length;
            keys=new int[genlen+extralen];
            System.arraycopy(genericKeys, 0, keys, 0, genlen);
            System.arraycopy(extraKeys, 0, keys, genlen, extralen);
        }
        Date fileDate=null;
        for (Object oDirectory : directories) {
            Directory directory=(Directory)oDirectory;
            for (int i = 0; directory != null && fileDate==null && i < keys.length; i++) {
                Date oneDate = directory.getDate(keys[i]);
                if (oneDate != null) {
                    fileDate = oneDate;
                    System.out.println("  getDateFromDirectory " + directory.getName() + " tag " + keys[i] + " value: " + oneDate);
                }
            }
        }
        return fileDate;
    }


}
