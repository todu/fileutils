/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.da.media.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author
 */
public class StrUtils {
    
    public static int atoi(String val) {
        return atoi(val, 0);
    }
    public static int atoi(String val,int defValue) {
        int retVal = defValue;
        try {
            retVal = Integer.parseInt(val);
        } catch (Exception e) {
            
        }
        return retVal;
    }
    
    public static String formatDateYYYMMDD(Date date) {
        String dateFormated = null;
        if (date!=null) {
            dateFormated = new SimpleDateFormat("yyyyMMdd").format(date);
        }
        return dateFormated;
    }
    
    public static String getFromLast(String textWhereSearch, String separator) {
    	int sepPos = textWhereSearch.lastIndexOf(separator);
    	String lastPart = sepPos<0?textWhereSearch:textWhereSearch.substring(sepPos+1);
    	return lastPart;
    }
}
