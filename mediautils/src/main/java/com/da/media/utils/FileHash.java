package com.da.media.utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.util.logging.Logger;

/**
 * calculates hash for an input stream or file
 * hash is a string
 */
public class FileHash {

    final static Logger log = Logger.getLogger(FileHash.class.getSimpleName());

    public static String calcImageMd5(String filePath) {
        File file=new File(filePath);
        return calcImageMd5(file);
    }

    public static String calcImageMd5(File file) {
        String hash=null;
        try {
            long start = System.currentTimeMillis();
            BufferedImage bif = ImageIO.read(file);
            DataBufferByte data = (DataBufferByte) bif.getRaster().getDataBuffer();

            byte[]bdata = data.getData();
            hash=calcMd5(bdata);
            log.info("calcImageMd5 time: "+(System.currentTimeMillis()-start)+" result: "+hash);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hash;
    }

    private static String calcFileMd5(File file) {
        String hash=null;
        try {
            long start = System.currentTimeMillis();
            byte[]bdata= Files.readAllBytes(file.toPath());
            hash=calcMd5(bdata);
            log.info("calcFileMd5 time: "+(System.currentTimeMillis()-start)+" result: "+hash);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hash;
    }

    /**
     * returns 32 lenth hash based on md5 algorithn
     */
    public static String calcMd5(byte[]bytes) {
        String hashText=null;
        try {
            long start = System.currentTimeMillis();
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[]digested=md.digest(bytes);
            final int positive = 1; // negative is -1
            BigInteger number = new BigInteger(positive, digested);
            hashText=number.toString(16); // convert to base 16
            log.info("calcMd5 time: "+(System.currentTimeMillis()-start)+" result: "+hashText);
        } catch (Exception e) {
            log.warning("calcMd5 unexpected error: "+e.getMessage());
            e.printStackTrace();
        }
        return hashText;
    }

    /**
     * compares only non null bytes[] and returns true if non null byte[] are equal
     *
     *
     */
    private static boolean compare(byte[]bytes1, byte[]bytes2) {
        boolean areEqual=false;
        try {
            long start = System.currentTimeMillis();
            if (bytes1!=null && bytes2!=null) {
                // ending zeroes should be ignored??
                if (bytes1.length==bytes2.length) {
                    areEqual=true;
                    for (int i=0;i<bytes1.length;i++) {
                        if (bytes1[i]!=bytes2[i]) {
                            areEqual=false;
                            break;
                        }
                    }
                }
            }
            log.info("compare time: "+(System.currentTimeMillis()-start)+" result: "+areEqual);
        } catch (Exception e) {
            log.warning("calcMd5 unexpected error: "+e.getMessage());
            e.printStackTrace();
        }
        return areEqual;
    }



}
