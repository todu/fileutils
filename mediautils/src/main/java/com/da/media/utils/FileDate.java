package com.da.media.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileDate {

    final static Logger log = Logger.getLogger(FileDate.class.getSimpleName());
    /**
     * Structures searched:<br/>
     * a) .../yyyymmdd/file<br/>
     * b) .../yymmdd/file<br/>
     *
     * @return
     */
    public static Date getDateFromPath(String path) {
        Date theDate = null;
        String separator = File.pathSeparator;
        if (path.indexOf(separator) >= 0) {

        } else if (path.indexOf("/") >= 0) {
            separator = "/";
        } else {
            separator = "\\\\";
        }
        String[] parts = path.split(separator);
        if (parts.length > 1) {
            String folder = parts[parts.length - 2];
            String year = null;
            String month = null;
            String day = null;
            if (folder != null && folder.length() == 8) {
                year = folder.substring(0, 4);
                month = folder.substring(4, 6);
                day = folder.substring(6);
            } else if (folder != null && folder.length() == 6) {
                year = folder.substring(0, 2);
                month = folder.substring(2, 4);
                day = folder.substring(4);
            }
            if (year != null) {
                Calendar cal = Calendar.getInstance();
                int iyear = StrUtils.atoi(year);
                int imonth = StrUtils.atoi(month);
                int iday = StrUtils.atoi(day);
                if (iyear != 0 && imonth != 0 && iday != 0) {
                    cal.set(iyear, imonth, iday);
                    theDate = new Date();
                }
            }
        }
        return theDate;
    }

    public static Date getDateFromFile(String path) {
        Date theDate = null;
        try {
            Path p = Paths.get(path);
            BasicFileAttributes view = Files.getFileAttributeView(p,
                    BasicFileAttributeView.class).readAttributes();
            if (view.isRegularFile()) {
                FileTime fCreationTime = view.creationTime();
                long crea = fCreationTime.to(TimeUnit.MILLISECONDS);
                Calendar dcrea = Calendar.getInstance();
                dcrea.setTimeInMillis(crea);
                Date datUpda = dcrea.getTime();

                FileTime fUpdateTime = view.lastModifiedTime();
                long upda = fUpdateTime.to(TimeUnit.MILLISECONDS);
                Calendar dupda = Calendar.getInstance();
                dupda.setTimeInMillis(upda);
                Date datCrea = dupda.getTime();

                if (crea < upda) {
                    theDate = datUpda;
                } else {
                    theDate = datCrea;
                }

//                log.trace("getDateFromFile path(" + path + ")creation("
//                        + fCreationTime.toString() + ")update("
//                        + fUpdateTime.toString() + ")");
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                System.out.println("getDateFromFile creation: "
                        + sdf.format(datCrea) + " update: "
                        + sdf.format(datUpda) + " olderDate: "
                        + sdf.format(theDate));
            }
        } catch (IOException ex) {
            Logger.getLogger(FileDate.class.getName()).log(Level.SEVERE, null,
                    ex);
        }
        return theDate;
    }


    public static Date getDateFromMetadata() {
        // TODO: pending
        return null;
    }
}
