package com.da.media.utils.test;

import com.da.media.utils.FileHash;

public class FileHashTest {

    public static void main(String[]params) {
        log("*****   main start *******");
        byte[]bytes=new byte[1024];
        for (int i=0;i<1024;bytes[i++]=97);
        String hash= FileHash.calcMd5(bytes);
        log(hash);
        log("length: "+hash.length());
        for (int i=0;i<1024;bytes[i++]=0);
        hash=FileHash.calcMd5(bytes);
        log(hash);
        log("length: "+hash.length());
        String file="/home/tomas/Downloads/IMG_20170915_082820.jpg";
        hash=FileHash.calcImageMd5(file);
        log(hash);
        log("length: "+hash.length());
        if ("f6834ebf570b7dbc69b201d8ad08796d".equals(hash)) {
            log ("Result is OK   :)");
        }
        log("*****   main end *******");
    }

    private static void log(String text) {
        System.out.println(text);
    }
}
