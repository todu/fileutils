package com.da.fileutils.poc;

import com.da.media.utils.GpsUtils;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.exif.GpsDirectory;
import com.drew.metadata.file.FileSystemDirectory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

/**
 * read immage gps
 * example from https://stackoverflow.com/questions/13559551/get-date-taken-of-an-image
 * and code https://github.com/drewnoakes/metadata-extractor
 */
public class JavaxtGPS {

    public static void main(String[]params) {
        String fileName="/home/tomas/Downloads/IMG_20170915_082820.jpg";
        String impath="/home/tomas/workspace/intellij/fileutils/mediautils/src/test/resources/images/";
        String fileName1="/home/tomas/Downloads/IMG_20170915_082820.jpg";
        String fileName2=impath+"IMG_20180303_192012-fotoCamaraXiaomi.jpg";
        String fileName3=impath+"IMG-20180302-WA0008-watsappimage.jpg";
        String fileName4=impath+"IMG-20180303-WA0001-watsappReceivedEva.jpg";

        fileName = fileName2;
        JavaxtGPS jg=new JavaxtGPS();
        jg.readGps(fileName);
        Date date = jg.getDate(fileName);
        System.out.println("Date of file: "+date);
        imagesDist(fileName1, fileName2);
        jg.readDirectories(fileName);
    }

    private static void imagesDist(String img1, String img2) {
        GeoLocation geoLocation1=getGeoLocation(img1);
        GeoLocation geoLocation2=getGeoLocation(img2);
        double distance=GpsUtils.distance(geoLocation1, geoLocation2);
        System.out.println("Distance: "+distance+" Km");
        System.out.println("url 1: "+GpsUtils.buildMapsUrl(geoLocation1));
        System.out.println("url 2: "+GpsUtils.buildMapsUrl(geoLocation2));
    }

    private static GeoLocation getGeoLocation(String fileName) {
        GeoLocation geoLocation=null;
        try {
            Collection<PhotoLocation> photoLocations = new ArrayList<PhotoLocation>();
            File file = new File(fileName);
            Metadata metadata = com.drew.imaging.ImageMetadataReader.readMetadata(file);

            Collection<GpsDirectory> gpsDirectories = metadata.getDirectoriesOfType(GpsDirectory.class);
            for (GpsDirectory gpsDirectory : gpsDirectories) {
                // Try to read out the location, making sure it's non-zero
                GeoLocation aGeoLocation = gpsDirectory.getGeoLocation();
                if (aGeoLocation != null && !aGeoLocation.isZero()) {
                    geoLocation=aGeoLocation;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return geoLocation;
    }

    public void readTool(String fileName) {

    }

    /**
     * example read gps using metadata-extractor
     * https://github.com/drewnoakes/metadata-extractor/blob/master/Samples/com/drew/metadata/GeoTagMapBuilder.java
     *
     * directory 5 name: GPS
     directory 5 class: com.drew.metadata.exif.GpsDirectory
     directory 5 toString: GPS Directory (9 tags)
     directory name: GPS
     directory class: com.drew.metadata.exif.GpsDirectory
     GPS 1 tagName; GPS Altitude Ref, type: 5, desc: Sea level
     GPS 2 tagName; GPS Latitude Ref, type: 1, desc: N
     GPS 3 tagName; GPS Latitude, type: 2, desc: 41° 25' 25.69"
     GPS 4 tagName; GPS Longitude Ref, type: 3, desc: E
     GPS 5 tagName; GPS Longitude, type: 4, desc: 2° 10' 56.71"
     GPS 6 tagName; GPS Altitude, type: 6, desc: 88 metres
     GPS 7 tagName; GPS Time-Stamp, type: 7, desc: 06:28:11.000 UTC
     GPS 8 tagName; GPS Processing Method, type: 27, desc: ASCII
     GPS 9 tagName; GPS Date Stamp, type: 29, desc: 2017:09:15
     */
    public void readGps(String fileName) {
        try {
            Collection<PhotoLocation> photoLocations = new ArrayList<PhotoLocation>();
            File file = new File(fileName);
            Metadata metadata = com.drew.imaging.ImageMetadataReader.readMetadata(file);

            Collection<GpsDirectory> gpsDirectories = metadata.getDirectoriesOfType(GpsDirectory.class);
            for (GpsDirectory gpsDirectory : gpsDirectories) {
                // Try to read out the location, making sure it's non-zero
                GeoLocation geoLocation = gpsDirectory.getGeoLocation();
                if (geoLocation != null && !geoLocation.isZero()) {
                    // Add to our collection for use below
                    photoLocations.add(new PhotoLocation(geoLocation, file));
                    System.out.println("geolocation: "+geoLocation.toDMSString());
                    System.out.println("altitude: "+gpsDirectory.getDescription(6));
                    Object altitude=gpsDirectory.getObject(6);
                    System.out.println("altitude: "+gpsDirectory.getObject(6).getClass().getName());
                    System.out.println("altitude: "+gpsDirectory.getInt(6));
                    com.drew.lang.Rational rational=(com.drew.lang.Rational)altitude;
                    System.out.println("altitude: "+rational.intValue());
                    Date gpsDate=gpsDirectory.getGpsDate();
                    System.out.println("gps date: "+gpsDate);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Date getDate(String fileName) {
        Date fileDate=null;
        try {
            File file = new File(fileName);
            Metadata metadata = com.drew.imaging.ImageMetadataReader.readMetadata(file);
            Collection<?> dirs=metadata.getDirectoriesOfType(ExifSubIFDDirectory.class);
            Date oneDate=getDateFromDirectory(dirs, null);
            if (oneDate==null) {
                // image capture date
                dirs=metadata.getDirectoriesOfType(ExifIFD0Directory.class);
                oneDate=getDateFromDirectory(dirs, null);
            }
            if (oneDate==null) {
                // gps date
                dirs=metadata.getDirectoriesOfType(GpsDirectory.class);
                oneDate=getDateFromDirectory(dirs, null);
            }
            if (oneDate==null) {
                // file system modification date
                dirs=metadata.getDirectoriesOfType(FileSystemDirectory.class);
                oneDate=getDateFromDirectory(dirs, new int[]{3});
            }
            fileDate=oneDate;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fileDate;
    }

    /**
     * https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html
     * Tag ID	Tag Name	Writable	Group	Values / Notes
     * 0x010f	Make	string	IFD0
     *    0x010f = 271
     * 0x0110	Model	string	IFD0
     *    0x0110 = 272
     * 0x0132	ModifyDate	string	IFD0	(called DateTime by the EXIF spec.)
     *    0x0132 = 306
     * 0x82aa	MDPrepDate	no	-
     *    0x82aa = 33450
     * 0x882a	TimeZoneOffset	int16s[n]	ExifIFD	(1 or 2 values: 1. The time zone offset of DateTimeOriginal from GMT in hours, 2. If present, the time zone offset of ModifyDate)
     *    0x882a = 34858
     * 0x9003	DateTimeOriginal	string	ExifIFD	(date/time when original image was taken)
     *    0x9003 = 36867
     * 0x9004	CreateDate	string	ExifIFD	(called DateTimeDigitized by the EXIF spec.)
     *    0x9004 = 36868
     * 0x9010	OffsetTime	string	ExifIFD	(time zone for ModifyDate)
     *    0x9010 = 36880
     * 0x9011	OffsetTimeOriginal	string	ExifIFD	(time zone for DateTimeOriginal)
     *    0x9011 = 36881
     * 0x9012	OffsetTimeDigitized	string	ExifIFD	(time zone for CreateDate)
     *    0x9012 = 36882
     * 0xc71b	PreviewDateTime	string!	IFD0
     *    0xc71b = 50971
     *
     * In FileSystemDirectory, type 3 can describe modified date
     */
    private Date getDateFromDirectory(Collection<?> directories, int[]extraKeys) {
        final int[]genericKeys={29, 0x0132, 0x9003, 0x9004, 0x82aa};
        int[]keys;
        if (extraKeys==null) {
            keys=genericKeys;
        } else {
            int genlen=genericKeys.length;
            int extralen=extraKeys.length;
            keys=new int[genlen+extralen];
            System.arraycopy(genericKeys, 0, keys, 0, genlen);
            System.arraycopy(extraKeys, 0, keys, genlen, extralen);
        }
        Date fileDate=null;
        for (Object oDirectory : directories) {
            Directory directory=(Directory)oDirectory;
            for (int i = 0; directory != null && fileDate==null && i < keys.length; i++) {
                Date oneDate = directory.getDate(keys[i]);
                if (oneDate != null) {
                    fileDate = oneDate;
                    System.out.println("  getDateFromDirectory " + directory.getName() + " tag " + keys[i] + " value: " + oneDate);
                }
            }
        }
        return fileDate;
    }

    public void readDirectories(String fileName) {
        try {
            Collection<PhotoLocation> photoLocations = new ArrayList<PhotoLocation>();
            File file = new File(fileName);
            Metadata metadata = com.drew.imaging.ImageMetadataReader.readMetadata(file);

            Iterable<Directory> directories=metadata.getDirectories(); //irectoriesOfType(GpsDirectory.class);
            int count=0;
            for (Directory directory : directories) {
                count++;
                System.out.println("directory "+count+" ************************ ");
                System.out.println("directory "+count+" name: "+directory.getName());
                System.out.println("directory "+count+" class: "+directory.getClass().getName());
                System.out.println("directory "+count+" toString: "+directory.toString());
                readExif(directory);
                System.out.println("directory "+count+" ************************ ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * https://sno.phy.queensu.ca/~phil/exiftool/TagNames/EXIF.html
     * Tag ID	Tag Name	Writable	Group	Values / Notes
     * 0x010f	Make	string	IFD0
     *    0x010f = 271
     * 0x0110	Model	string	IFD0
     *    0x0110 = 272
     * 0x0132	ModifyDate	string	IFD0	(called DateTime by the EXIF spec.)
     *    0x0132 = 306
     * 0x82aa	MDPrepDate	no	-
     *    0x82aa = 33450
     * 0x882a	TimeZoneOffset	int16s[n]	ExifIFD	(1 or 2 values: 1. The time zone offset of DateTimeOriginal from GMT in hours, 2. If present, the time zone offset of ModifyDate)
     *    0x882a = 34858
     * 0x9003	DateTimeOriginal	string	ExifIFD	(date/time when original image was taken)
     *    0x9003 = 36867
     * 0x9004	CreateDate	string	ExifIFD	(called DateTimeDigitized by the EXIF spec.)
     *    0x9004 = 36868
     * 0x9010	OffsetTime	string	ExifIFD	(time zone for ModifyDate)
     *    0x9010 = 36880
     * 0x9011	OffsetTimeOriginal	string	ExifIFD	(time zone for DateTimeOriginal)
     *    0x9011 = 36881
     * 0x9012	OffsetTimeDigitized	string	ExifIFD	(time zone for CreateDate)
     *    0x9012 = 36882
     * 0xc71b	PreviewDateTime	string!	IFD0
     *    0xc71b = 50971
     *
     * 271, 272 -> tool, model
     */
    public void readExif(Directory directory) {
        String dirName=directory.getName();
        System.out.println("directory name: "+dirName);
        System.out.println("directory class: "+directory.getClass().getName());
        Collection<Tag> tags = directory.getTags();
        Iterator<Tag> iterator = tags.iterator();
        int count=0;
        while (iterator.hasNext()) {
            count++;
            Tag t=iterator.next();
            String name=t.getTagName();
            int type=t.getTagType();
            String desc=t.getDescription();
            System.out.println("        "+dirName+" "+count+" tagName; "+name+", type: "+type+", desc: "+desc);
        }

    }

    private class PhotoLocation {
        public PhotoLocation(GeoLocation geoLocation, File file) {

        }
    }
}
