package com.da.data.h2.test;

import com.da.data.h2.DBConnection;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnectionTest {

    @Test
    public void testOpenConnection() {
        DBConnection myconn = new DBConnection();
        myconn.setDatabasePath("/home/tomas/workspace/foto/data/fotos");
        Connection conn = myconn.getConnection();
        boolean instant=myconn.runInstan("select * from file");
        System.out.println("instant: "+instant);
        int update=myconn.executeUpdate("update file set hash='someash2' where path='somepath2'");
        System.out.println("update: "+update);
        ResultSet rset = myconn.executeQuery("select * from file");
        int len=-1;
        try {
            rset.last();
            len=rset.getRow();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("query len: "+len);
        myconn.closeConnection();

    }
    @Test
    public void testListFile() {
        DBConnection myconn = new DBConnection();
        myconn.setDatabasePath("/home/tomas/workspace/foto/data/fotos");
        Connection conn = myconn.getConnection();
        boolean instant=myconn.runInstan("select * from file");
        System.out.println("instant: "+instant);
        int update=myconn.executeUpdate("update file set hash='someash2' where path='somepath2'");
        System.out.println("update: "+update);
        ResultSet rset = myconn.executeQuery("select * from file");
        int len=-1;
        try {
            rset.last();
            len=rset.getRow();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("query len: "+len);
        myconn.closeConnection();

    }
}
