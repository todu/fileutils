-- create database to manage immages
--
-- files repository

create table file (
    id bigint auto_increment,
    path varchar2(2048),
    hash varchar2(128),
    lloc varchar2(256)
);

create unique index filePrimary on file (id);
create index filehash on file (hash);

-- taula que enmagatzema
-- http://www.h2database.com/html/datatypes.html
-- date formats: date, time, timestamp,
create table proceslog (
    processid varchar2(12),
    processtime timestamp,
    path varchar2(2048),
    hash varchar2(128),

);

