package com.da.data.h2;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class DBConnection {

    private static final String DB_DRIVER = "org.h2.Driver";
//    private static final String DB_CONNECTION = "jdbc:h2:/home/tomas/workspace/intellij/fileutils/h2utils/data/test";
    private static final String DB_CONNECTION = "jdbc:h2:";
    private static final String DB_USER = "sa";

    private String databasePath;

    Connection myConnection;


    /**
     * Returns a connection to {@link #databasePath}
     * @return
     */
    public Connection getConnection() {
        if (myConnection==null) {
            myConnection=openDbdConnection();
        }
        return myConnection;
    }

    public void closeConnection() {
        try {
            if (myConnection != null) {
                myConnection.close();
                myConnection = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDatabasePath(String databasePath) {
        this.databasePath=databasePath;
    }

    /**
     * https://www.tutorialspoint.com/h2_database/h2_database_jdbc_connection.htm
     */
    private Connection openDbdConnection() {
        // 1. retister jdbc database driver
        try {
            Class.forName (DB_DRIVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // 2. open connection
        Connection connection = null;
        try {
            String hdbcPath=DB_CONNECTION+databasePath;
            connection = java.sql.DriverManager.getConnection(hdbcPath, DB_USER, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return connection;
    }

    public boolean runInstan(String sql) {
        Connection conn=this.getConnection();
        boolean result=false;
        try {
            Statement stm = conn.createStatement();
            result=stm.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int executeUpdate(String sql) {
        Connection conn=this.getConnection();
        int result=-1;
        try {
            Statement stm = conn.createStatement();
            result=stm.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public ResultSet executeQuery(String sql) {
        Connection conn=this.getConnection();
        ResultSet result=null;
        try {
            Statement stm = conn.createStatement();
            result=stm.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


}
